
## Why I am in this Course?
 
Recent Computational capabilities have enabled generation and storage of huge amount of data in almost every field build on the internet. This data can be studied to analyse different patterns of user behaviour or event impacts. Through this course, I want to gain understanding of the fundamentals of data visualization techniques and get familiarized with the standards of the field. 
